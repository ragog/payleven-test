import data.Credentials;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.junit.Rule;
import org.junit.rules.TestName;


import org.openqa.selenium.WebElement;
import screen.Payleven;
import util.AppiumDriverBuilder;
import util.Device;

import static data.Credentials.TEST_SYSTEM_VALID;
import static java.lang.System.out;
import static util.AppiumDriverBuilder.forAndroid;
import static util.Device.htc_one;

/**
 * Created by grago on 15/06/15.
 */
public abstract class AbstractTest {

    @Rule
    public TestName testName = new TestName();
    //@Rule
    //public TestObjectTestResultWatcher watcher = new TestObjectTestResultWatcher();

    private AndroidDriver driver;
    private final Device device;

    protected Payleven payleven;

    public AbstractTest() {
        this.device = htc_one;
    }

    public AbstractTest(Device device) {
        this.device = device;
    }

    public void connect() {
        out.println("Device: " + device.name);
        driver = forAndroid().againstLocalhost().build();
        //IOSDriver driver = AppiumDriverBuilder.forIOS().againstTestobject("C08B366D724C44A5AFEA5DAB78C7E0F0", 3, device.name, this.getClass().getName(), testName.getMethodName()).build();
        //IOSDriver driver = AppiumDriverBuilder.forIOS().againstLocalhost().build();
        //IOSDriver driver = AppiumDriverBuilder.forIOS().againstHost("192.168.178.41", 4723).build();

        payleven = new Payleven(device, driver);
        //this.watcher.setAppiumDriver(driver);
    }

    public void login() {
        if (payleven.loginScreen().isLoggedIn() == false) {
            payleven.loginScreen().login(TEST_SYSTEM_VALID);
        }
    }

    public void disconnect() {
        driver.quit();
    }

    /*
    public void searchAndOpenReport(String crefoNumber, Product product){
        payeleven.menu().search();

        payeleven.searchScreen().search(new Search().withCrefoNumber(crefoNumber));
        Optional<SearchResult.Item> searchResultItem = payeleven.searchScreen().results().findFirst();
        assertTrue(searchResultItem.isPresent());
        payeleven.searchScreen().select(searchResultItem.get());

        payeleven.legitimateInterest().selectProduct(product);
    }
    */

}
