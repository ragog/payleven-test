import data.Credentials;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.Device;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by grago on 16/06/15.
 */
public class LoginTest extends AbstractTest {

    public LoginTest() {
        super(Device.htc_one);
    }

    protected LoginTest(Device device) {
        super(device);
    }

    @Before
    public void setup() {
        connect();
    }

    @Test
    public void testLoginWithValidCredentials() throws InterruptedException {
        payleven.loginScreen().login(Credentials.TEST_SYSTEM_VALID);

        assertTrue(payleven.loginScreen().isLoggedIn());
    }

    @Test
    public void testLoginWithInvalidCredentials() throws InterruptedException {
        payleven.loginScreen().login(Credentials.TEST_SYSTEM_INVALID);

        assertFalse(payleven.loginScreen().isLoggedIn());
    }

    @Test
    public void testLoginWithInvalidEmail() throws InterruptedException {
        payleven.loginScreen().login(Credentials.TEST_SYSTEM_INVALID_EMAIL);

        assertFalse(payleven.loginScreen().isLoggedIn());
    }

    @Test
    public void testLoginWithInvalidPassword() throws InterruptedException {
        payleven.loginScreen().login(Credentials.TEST_SYSTEM_INVALID_PASSWORD);

        assertFalse(payleven.loginScreen().isLoggedIn());
    }

    /*
    @Test
    public void testLoginWithEmptyCredentials() throws InterruptedException {
        payeleven.loginScreen().login(Credentials.TEST_SYSTEM_EMPTY);

        assertFalse(payeleven.loginScreen().isLoggedIn());
    }

    @Test
    public void testLoginWithEmptyEmail() throws InterruptedException {
        payeleven.loginScreen().login(Credentials.TEST_SYSTEM_EMPTY_EMAIL);

        assertFalse(payeleven.loginScreen().isLoggedIn());
    }

    @Test
    public void testLoginWithEmptyPassword() throws InterruptedException {
        payeleven.loginScreen().login(Credentials.TEST_SYSTEM_EMPTY_PASSWORD);

        assertFalse(payeleven.loginScreen().isLoggedIn());
    }
    */

    @After
    public void terminate(){
        disconnect();
    }

}
