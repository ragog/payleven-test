package screen;

import io.appium.java_client.AppiumDriver;
import util.Device;

/**
 * Created by grago on 16/06/15.
 */
public class Payleven {

    private final Device device;
    private final AppiumDriver driver;

    public Payleven(Device device, AppiumDriver driver){
        this.device = device;
        this.driver = driver;
    }

    public LoginScreen loginScreen() { return new LoginScreen(driver, device); }

}
