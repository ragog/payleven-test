package screen;

import data.Credentials;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.AbstractScreen;
import util.Device;

/**
 * Created by grago on 16/06/15.
 */
public class LoginScreen extends AbstractScreen {

    @AndroidFindBy(id = "de.payleven.androidphone:id/sign_in_button")
    private MobileElement signInButton;

    @AndroidFindBy(id = "de.payleven.androidphone:id/email_edittext")
    private MobileElement emailField;

    @AndroidFindBy(id = "de.payleven.androidphone:id/password_edittext")
    private MobileElement passwordField;

    @AndroidFindBy(id = "android:id/message")
    private MobileElement signInDialog;

    @AndroidFindBy(id = "de.payleven.androidphone:id/sign_up_button")
    private MobileElement signUpButton;

    public LoginScreen(AppiumDriver driver, Device device) {
        super(driver, device);
    }

    public void login(Credentials credentials){

        /* Fill in e-mail field. */
        emailField.sendKeys(credentials.email);

        /* Switch focus to the password field and fill it in. */
        passwordField.click();
        passwordField.sendKeys(credentials.password);

        /* Hide keyboard to reveal the login button. */
        driver.hideKeyboard();

        /* Click the login button. */
        signInButton.click();

    }

    public void signup(){

        /* Click the signup button to access the country selection screen. */
        signUpButton.click();

    }

    public boolean isLoggedIn() {

        try{
            // if sign in error dialog is visible we are not logged in
            return (!signInDialog.isEnabled());
        } catch (NoSuchElementException e){
            return true;
        }

    }

    public boolean isSigningUp() {

        try{
            WebElement registerButton = new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.name("Register")));
            return (registerButton.isEnabled());
        } catch (NoSuchElementException e){
            return false;
        }

    }

}
