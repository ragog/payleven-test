package util;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.dd.plist.PropertyListParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by grago on 16/06/15.
 */
public class Localizer {

    enum Locale {
        DE,EN,FR
    }

    private static Locale LOCALE = Locale.EN;

    public static String localize(String in) {
        Pattern pattern = Pattern.compile("\\{\\{(.*)\\}\\}");
        Matcher m = pattern.matcher(in);
        StringBuffer out = new StringBuffer(in.length());
        while (m.find()) {
            String key = m.group(1);
            m.appendReplacement(out, Matcher.quoteReplacement(translate(key)));
        }
        m.appendTail(out);
        return out.toString();
    }

    private static String translate(String key) {
        try{
            NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(Localizer.class.getResourceAsStream(LOCALE.name().toLowerCase() + ".lproj/Localizable.strings"));
            NSString value = (NSString) rootDict.objectForKey(key);

            return value.getContent();
        } catch (Exception e) {
            throw new RuntimeException("key: " + key + " " + e.getMessage(), e);
        }
    }
}
