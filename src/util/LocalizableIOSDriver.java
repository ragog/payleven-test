package util;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Response;

import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by grago on 16/06/15.
 */
public class LocalizableIOSDriver extends IOSDriver {

    public LocalizableIOSDriver(URL remoteAddress, Capabilities desiredCapabilities) {
        super(remoteAddress, desiredCapabilities);
    }

    @Override
    protected List<WebElement> findElements(String by, String using) {
        System.out.print(Thread.currentThread() +  "find by " + by + ": " + using + " --> ");
        using = Localizer.localize(using);
        System.out.println("(" + by + ") " + using);

        return super.findElements(by, using);
    }

    public Response execute(String driverCommand, Map<String, ?> parameters) {
        System.out.print(Thread.currentThread() + "(" + driverCommand + ") " + parameters + " --> ");

        Response response = super.execute(driverCommand, parameters);

        System.out.println(response);

        return response;
    }

}