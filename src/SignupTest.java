import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.Device;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by grago on 16/06/15.
 */
public class SignupTest extends AbstractTest {

    public SignupTest(){
        super(Device.htc_one);
    }

    @Before
    public void setup() {
        connect();
    }

    @Test
    public void testSignupCountryScreenAccess() throws InterruptedException {
        payleven.loginScreen().signup();

        assertTrue(payleven.loginScreen().isSigningUp());
    }

    @After
    public void terminate(){
        disconnect();
    }

}
