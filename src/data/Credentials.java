package data;

/**
 * Created by grago on 15/06/15.
 */
public enum Credentials {

    TEST_SYSTEM_VALID("account@testobject.com", "abcd1234"),
    TEST_SYSTEM_INVALID("00z83kHg", "it9pYB27"),
    TEST_SYSTEM_INVALID_EMAIL("00z83kHg", "abcd1234"),
    TEST_SYSTEM_INVALID_PASSWORD("account@testobject.com", "00z83kHg"),
    TEST_SYSTEM_EMPTY("", ""),
    TEST_SYSTEM_EMPTY_EMAIL("", "00z83kHg"),
    TEST_SYSTEM_EMPTY_PASSWORD("account@testobject.com", "");

    public final String email;
    public final String password;

    Credentials(String membershipNumber, String generalPassword) {
        this.email = membershipNumber;
        this.password = generalPassword;
    }
}
